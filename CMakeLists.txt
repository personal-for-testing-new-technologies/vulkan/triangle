cmake_minimum_required(VERSION 3.27.0)

project("triangle" VERSION 1.0 LANGUAGES CXX)

add_compile_options(
    -Werror

    -Wall
    -Wextra
    -Wpedantic

    -Wcast-align
    -Wcast-qual
    -Wconversion
    -Wctor-dtor-privacy
    -Wenum-compare
    -Wfloat-equal
    -Wnon-virtual-dtor
    -Wold-style-cast
    -Woverloaded-virtual
    -Wredundant-decls
    -Wsign-conversion
    -Wsign-promo

    -O2
)

add_link_options(
     -lglfw 
     -lvulkan 
     -ldl 
     -lpthread 
     -lX11 
     -lXxf86vm 
     -lXrandr
     -lXi
    )

set(SOURCE_FILES 
  main.cpp
  )

if(NOT CMAKE_CXX_EXTENSIONS)
    set(CMAKE_CXX_EXTENSIONS OFF)
endif()

add_executable( triangle
  ${SOURCE_FILES}
  )
